require 'rails_helper'

RSpec.describe "notes/new", type: :view do
  before(:each) do
    assign(:note, Note.new(
      :user => nil,
      :message => "MyText",
      :noteable_id => 1,
      :noteable_type => "MyString"
    ))
  end

  it "renders new note form" do
    render

    assert_select "form[action=?][method=?]", notes_path, "post" do

      assert_select "input[name=?]", "note[user_id]"

      assert_select "textarea[name=?]", "note[message]"

      assert_select "input[name=?]", "note[noteable_id]"

      assert_select "input[name=?]", "note[noteable_type]"
    end
  end
end
