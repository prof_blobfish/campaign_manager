require 'rails_helper'

RSpec.describe "notes/index", type: :view do
  before(:each) do
    assign(:notes, [
      Note.create!(
        :user => nil,
        :message => "MyText",
        :noteable_id => 2,
        :noteable_type => "Noteable Type"
      ),
      Note.create!(
        :user => nil,
        :message => "MyText",
        :noteable_id => 2,
        :noteable_type => "Noteable Type"
      )
    ])
  end

  it "renders a list of notes" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Noteable Type".to_s, :count => 2
  end
end
