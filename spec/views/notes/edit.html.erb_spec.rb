require 'rails_helper'

RSpec.describe "notes/edit", type: :view do
  before(:each) do
    @note = assign(:note, Note.create!(
      :user => nil,
      :message => "MyText",
      :noteable_id => 1,
      :noteable_type => "MyString"
    ))
  end

  it "renders the edit note form" do
    render

    assert_select "form[action=?][method=?]", note_path(@note), "post" do

      assert_select "input[name=?]", "note[user_id]"

      assert_select "textarea[name=?]", "note[message]"

      assert_select "input[name=?]", "note[noteable_id]"

      assert_select "input[name=?]", "note[noteable_type]"
    end
  end
end
