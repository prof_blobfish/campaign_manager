require 'rails_helper'

RSpec.describe "projects/new", type: :view do
  before(:each) do
    assign(:project, Project.new(
      :description => "MyText",
      :client => nil,
      :type => nil
    ))
  end

  it "renders new project form" do
    render

    assert_select "form[action=?][method=?]", projects_path, "post" do

      assert_select "textarea[name=?]", "project[description]"

      assert_select "input[name=?]", "project[client_id]"

      assert_select "input[name=?]", "project[type_id]"
    end
  end
end
