# == Schema Information
#
# Table name: historicals
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  item_id    :integer
#  item_type  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :valid_historical, class: Historical do
    association :user, factory: :valid_user
    association :item, factory: :valid_task
  end
end
