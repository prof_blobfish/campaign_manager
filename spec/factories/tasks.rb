# == Schema Information
#
# Table name: tasks
#
#  id            :integer          not null, primary key
#  name          :string
#  description   :text
#  schedule_date :datetime
#  complete_date :datetime
#  status_id     :integer
#  client_id     :integer
#  project_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :valid_task , class: Task do
    name "MyString"
    description "MyText"
    schedule_date "2018-01-04 20:53:22"
    complete_date "2018-01-04 20:53:22"
    association :status, factory: :valid_task_status
    association :client, factory: :valid_client
    association :project, factory: :valid_project
  end
end
