# == Schema Information
#
# Table name: types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :valid_type, class: Type do
    name "MyString"
  end
end
