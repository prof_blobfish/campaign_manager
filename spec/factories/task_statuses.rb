# == Schema Information
#
# Table name: task_statuses
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :valid_task_status, class: TaskStatus do
    name "im valid look at me"
  end
end
