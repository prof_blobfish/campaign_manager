# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  description :text
#  client_id   :integer
#  type_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :valid_project, class: Project do
    description "MyText"
    association :client, factory: :valid_client
    association :type, factory: :valid_type
  end
end
