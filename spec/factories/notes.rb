# == Schema Information
#
# Table name: notes
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  message       :text
#  noteable_id   :integer
#  noteable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :valid_note, class: Note do
    association :user, factory: :valid_user
    message "MyText"
    association :noteable, factory: :valid_task
  end
end
