# == Schema Information
#
# Table name: clients
#
#  id         :integer          not null, primary key
#  name       :string
#  phone      :string
#  email      :string
#  notes      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :valid_client, class: Client do
    name "Whisle teeth"
    phone "123456"
    email "whistleteeth@aol.com"
  end
end
