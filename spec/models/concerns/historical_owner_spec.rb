require 'rails_helper'

RSpec.describe HistoricalOwner, type: :model do
  before do
    @historical_models = []
    ApplicationRecord.descendants.each do |model|
      if model.included_modules.include?(HistoricalOwner)
        name = "valid_#{model.name.underscore}"
        @historical_models << create(name.to_sym)
      end
    end
    @owner = create(:valid_user)
  end

  describe "owner" do
    it "assigns a new owner" do
      @historical_models.each do |model|
        model.owner = @owner
        expect(model.save).to be true
        model.reload
        expect(model.owner).to eq(@owner)
      end
    end

    it "will not assign a new owner if id not from User" do
      @historical_models.each do |model|
        expect(model.valid?).to be true
        not_owner = create(:valid_client)
        model.owner = not_owner
        expect{model.save}.to_not change{model.historicals.count}
        expect(model.owner).to_not eq(not_owner)
      end
    end

    it "returns the last owner" do
      @historical_models.each do |model|
        owners = create_list(:valid_user, 3)
        owners.each do |owner|
          model.owner = owner
        end
        expect(model.owner).to eq(owners.last)
      end
    end
  end

  describe "owner_id" do
    it "assigns a new owner_id" do
      @historical_models.each do |model|
        model.owner_id = @owner.id
        expect(model.save).to be true
        model.reload
        expect(model.owner_id).to eq(@owner.id)
      end
    end

    it "returns the last owner_id" do
      @historical_models.each do |model|
        owners = create_list(:valid_user, 3)
        owners.each do |owner|
          model.owner_id = owner.id
        end
        expect(model.owner_id).to eq(owners.last.id)
      end
    end

    it "will not assign a new owner if id not from User" do
      @historical_models.each do |model|
        expect(model.valid?).to be true
        not_owner = create(:valid_client)
        model.owner_id = not_owner.id
        expect{model.save}.to_not change{model.historicals.count}
        expect(model.owner).to_not eq(not_owner)
      end
    end
  end

  describe "owner_history" do
    it "returns all the historicals sorted by created_at" do
      @historical_models.each do |model|
        historicals = create_list(:valid_historical, 3, item: model)
        expect(model.owner_history.first).to eq(historicals.first)
        expect(model.owner_history.last).to eq(historicals.last)
      end
    end
  end

  describe "historicals" do
    it "has_many" do
      @historical_models.each do |model|
        historicals = create_list(:valid_historical, 3, item: model)
        expect(model.historicals).to eq(historicals)
      end
    end
  end
end
