# == Schema Information
#
# Table name: clients
#
#  id         :integer          not null, primary key
#  name       :string
#  phone      :string
#  email      :string
#  notes      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Client, type: :model do
  describe "validations" do
    before do
      @client = build(:valid_client)
    end

    describe "email" do
      it "is invalid without a email" do
        @client.email = nil
        expect(@client.valid?).to be false
        expect(@client.errors[:email]).to eq(["can't be blank"])
      end

      it "is valid with a email" do
        @client.email = "some email"
        expect(@client.valid?).to be true
      end
    end

    describe "name" do
      it "is invalid without a name" do
        @client.name = nil
        expect(@client.valid?).to be false
        expect(@client.errors[:name]).to eq(["can't be blank"])
      end

      it "is valid with a name" do
        @client.name = "Whisle teeth"
        expect(@client.valid?).to be true
      end
    end

    describe "phone" do
      it "is invalid without a phone" do
        @client.phone = nil
        expect(@client.valid?).to be false
        expect(@client.errors[:phone]).to eq(["can't be blank"])
      end

      it "is valid with a phone" do
        @client.phone = "some phone"
        expect(@client.valid?).to be true
      end
    end

  end

  describe "relationships" do
    before do
      @client = create(:valid_client)
    end

    it "has many projects" do
      projects = create_list(:valid_project, 3, client: @client)
      expect(@client.projects).to eq projects
    end
  end
end
