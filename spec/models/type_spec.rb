# == Schema Information
#
# Table name: types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Type, type: :model do
  describe "validations" do
    before do
      @type = build(:valid_type)
    end

    describe "name" do
      it "is invalid without a name" do
        @type.name = nil
        expect(@type.valid?).to be false
        expect(@type.errors[:name]).to eq(["can't be blank"])
      end

      it "is invalid when client does not exist" do
        @type.client_id = 5555
        expect(@type.valid?).to be false
        expect(@type.errors[:type]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a name" do
        @type.name = "some name"
        expect(@type.valid?).to be true
      end
    end
  end
end
