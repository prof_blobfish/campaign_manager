# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  description :text
#  client_id   :integer
#  type_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Project, type: :model do
  describe "validations" do
    before do
      @project = build(:valid_project)
    end

    describe "client" do
      it "is invalid with client_id nil" do
        @project.client_id = nil
        expect(@project.valid?).to be false
        expect(@project.errors[:client]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when client does not exist" do
        @project.client_id = 5555
        expect(@project.valid?).to be false
        expect(@project.errors[:client]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a client" do
        client = create(:valid_client)
        @project.client = client
        expect(@project.valid?).to be true
      end
    end

    describe "type" do
      it "is invalid with type_id nil" do
        @project.type_id = nil
        expect(@project.valid?).to be false
        expect(@project.errors[:type]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when client does not exist" do
        @project.type_id = 5555
        expect(@project.valid?).to be false
        expect(@project.errors[:type]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a client" do
        type = create(:valid_type)
        @project.type = type
        expect(@project.valid?).to be true
      end
    end
  end
end
