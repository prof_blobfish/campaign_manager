# == Schema Information
#
# Table name: notes
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  message       :text
#  noteable_id   :integer
#  noteable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Note, type: :model do
  describe "validations" do
    before do
      @note = build(:valid_note)
    end

    describe "user" do
      it "is invalid with user_id nil" do
        @note.user_id = nil
        expect(@note.valid?).to be false
        expect(@note.errors[:user]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when user does not exist" do
        @note.user_id = 5555
        expect(@note.valid?).to be false
        expect(@note.errors[:user]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a user" do
        user = create(:valid_user)
        @note.user = user
        expect(@note.valid?).to be true
      end
    end

    describe "noteable" do
      it "is invalid when user does not exist" do
        @note.noteable_id = 5555
        @note.noteable_type = "Task"
        expect(@note.valid?).to be false
        expect(@note.errors[:noteable]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a user" do
        noteable = create(:valid_task)
        @note.noteable = noteable
        expect(@note.valid?).to be true
      end
    end

    describe "message" do
      it "is invalid without a message" do
        @note.message = nil
        expect(@note.valid?).to be false
        expect(@note.errors[:message]).to eq(["can't be blank"])
      end

      it "is valid with a message" do
        @note.message = "this msg"
        expect(@note.valid?).to be true
      end
    end

  end

  describe "relationships" do
    before do
      @note = create(:valid_note)
    end

    it "belongs to user" do
      user = create(:valid_user)
      @note.user = user
      expect(@note.save).to be true
      @note.reload
      expect(@note.user).to eq user
    end

    it "belongs to noteable" do
      noteable = create(:valid_task)
      @note.noteable = noteable
      expect(@note.save).to be true
      @note.reload
      expect(@note.noteable).to eq noteable
    end
  end
end
