# == Schema Information
#
# Table name: tasks
#
#  id            :integer          not null, primary key
#  name          :string
#  description   :text
#  schedule_date :datetime
#  complete_date :datetime
#  status_id     :integer
#  client_id     :integer
#  project_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Task, type: :model do
  describe "validations" do
    before do
      @task = build(:valid_task)
    end

    describe "client" do
      it "is invalid with user_id nil" do
        @task.client_id = nil
        expect(@task.valid?).to be false
        expect(@task.errors[:client]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when client does not exist" do
        @task.client_id = 5555
        expect(@task.valid?).to be false
        expect(@task.errors[:client]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a user" do
        client = create(:valid_client)
        @task.client = client
        expect(@task.valid?).to be true
      end
    end

    describe "project" do
      it "is invalid with project_id nil" do
        @task.project_id = nil
        expect(@task.valid?).to be false
        expect(@task.errors[:project]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when project does not exist" do
        @task.project_id = 5555
        expect(@task.valid?).to be false
        expect(@task.errors[:project]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a user" do
        project = create(:valid_project)
        @task.project = project
        expect(@task.valid?).to be true
      end
    end

    describe "status" do
      it "is invalid with status_id nil" do
        @task.status_id = nil
        expect(@task.valid?).to be false
        expect(@task.errors[:status]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when status does not exist" do
        @task.status_id = 5555
        expect(@task.valid?).to be false
        expect(@task.errors[:status]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a status" do
        status = create(:valid_task_status)
        @task.task_status = status
        expect(@task.valid?).to be true
      end
    end
  end

  describe "relationships" do
    before do
      @task = create(:valid_task)
    end

    it "belongs to client" do
      client = create(:valid_client)
      @task.client = client
      expect(@task.save).to be true
      @task.reload
      expect(@task.client).to eq client
    end

    it "belongs to project" do
      project = create(:valid_project)
      @task.project = project
      expect(@task.save).to be true
      @task.reload
      expect(@task.project).to eq project
    end

    it "belongs to status" do
      status = create(:valid_task_status)
      @task.task_status = status
      expect(@task.save).to be true
      @task.reload
      expect(@task.task_status).to eq status
    end

  end
end
