# == Schema Information
#
# Table name: historicals
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  item_id    :integer
#  item_type  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Historical, type: :model do
  describe "validations" do
    before do
      @historical = build(:valid_historical)
    end

    describe "user" do
      it "is invalid with user_id nil" do
        @historical.user_id = nil
        expect(@historical.valid?).to be false
        expect(@historical.errors[:user]).to eq(["can't be blank", "must exist"])
      end

      it "is invalid when user does not exist" do
        @historical.user_id = 5555
        expect(@historical.valid?).to be false
        expect(@historical.errors[:user]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a user" do
        user = create(:valid_user)
        @historical.user = user
        expect(@historical.valid?).to be true
      end
    end

    describe "item" do
      it "is invalid when user does not exist" do
        @historical.item_id = 5555
        @historical.item_type = "Task"
        expect(@historical.valid?).to be false
        expect(@historical.errors[:item]).to eq(["can't be blank", "must exist"])
      end

      it "is valid with a user" do
        item = create(:valid_task)
        @historical.item = item
        expect(@historical.valid?).to be true
      end
    end
  end
end
