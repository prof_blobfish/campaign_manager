# == Schema Information
#
# Table name: task_statuses
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe TaskStatus, type: :model do
  describe "validations" do
    before do
      @status = build(:valid_task_status)
    end

    describe "name" do
      it "is invalid without a name" do
        @status.name = nil
        expect(@status.valid?).to be false
        expect(@status.errors[:name]).to eq(["can't be blank"])
      end

      it "is valid with a name" do
        @status.name = "some name"
        expect(@status.valid?).to be true
      end
    end
  end
end
