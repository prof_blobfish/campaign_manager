# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

admin
  email
  password

user
  email
  password
  phone_number
  has_many historicals

client
  name
  phone_number
  email
  notes
  has_many projects
  owner --Historical
  followers --users

ProjectType -SEO, email, PPC, DirectMail, Social Media
  name
  description

Project
  ProjectType
  description
  client
  has_many tasks
  owner --Historical
  followers --users

Tasks
  name
  description
  schedule_date
  complete_date
  status
    complete
    incomplete
    blocked
  owner --Historical
  followers --users
  client
  Project

TaskStatus
  name

notes
  user
  message

Historical
  user
  item_id
  item_description
