Rails.application.routes.draw do
  resources :notes
  resources :tasks
  resources :projects
  resources :clients
  resources :types
  resources :task_statuses
  devise_for :admins
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'static#index'

end
