json.extract! type, :description :client_id, :type_id, :created_at
json.url project_url(project, format: :json)
