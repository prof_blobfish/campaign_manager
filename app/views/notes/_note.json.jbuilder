json.extract! note, :id, :user_id, :message, :noteable_id, :noteable_type, :created_at, :updated_at
json.url note_url(note, format: :json)
