json.extract! task, :id, :name, :description, :schedule_date, :complete_date, :status_id, :client_id, :project_id, :created_at, :updated_at
json.url task_url(task, format: :json)
