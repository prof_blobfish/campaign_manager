class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.all.sort_by { |t| t.complete_date }
    @task_statuses = TaskStatus.all.map{ |s| [s.name, s.id] }
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @clients = Client.all.map{ |c| [ c.name, c.id] }.sort
    @projects = Project.all.map{ |j| [ j.description, j.id] }.sort
    @task_statuses = TaskStatus.all.map{ |s| [s.name, s.id] }
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
    @clients = Client.all.map{ |c| [ c.name, c.id] }.sort
    @projects = Project.all.map{ |j| [ j.description, j.id] }.sort
    @task_statuses = TaskStatus.all.map{ |s| [s.name, s.id] }
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)
    @clients = Client.all.map{ |c| [ c.name, c.id] }.sort
    @projects = Project.all.map{ |j| [ j.description, j.id] }.sort
    @task_statuses = TaskStatus.all.map{ |s| [s.name, s.id] }

    respond_to do |format|
      if @task.save
        format.html { redirect_to @task, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to "/tasks", notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: "/tasks" }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:name, :description, :schedule_date, :complete_date, :status_id, :client_id, :project_id)
    end
end
