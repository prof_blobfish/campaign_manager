# == Schema Information
#
# Table name: historicals
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  item_id    :integer
#  item_type  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Historical < ApplicationRecord
  validates :user, presence: true
  validates :item, presence: true

  belongs_to :user
  belongs_to :item, polymorphic: true
end
