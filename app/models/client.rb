# == Schema Information
#
# Table name: clients
#
#  id         :integer          not null, primary key
#  name       :string
#  phone      :string
#  email      :string
#  notes      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Client < ApplicationRecord
  include HistoricalOwner

  validates :email, :name, :phone, presence: true

  has_many :projects
  has_many :tasks

  def start_time
    self.task.schedule_date
  end

  def end_time
    self.task.complete_date
  end
end
