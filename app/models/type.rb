# == Schema Information
#
# Table name: types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Type < ApplicationRecord
  include HistoricalOwner

  validates :name, :description, presence: true

  has_many :projects
end
