# == Schema Information
#
# Table name: tasks
#
#  id            :integer          not null, primary key
#  name          :string
#  description   :text
#  schedule_date :datetime
#  complete_date :datetime
#  status_id     :integer
#  client_id     :integer
#  project_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Task < ApplicationRecord
  include HistoricalOwner

  validates :client, presence: true
  validates :project, presence: true
  validates :status, presence: true

  belongs_to :client
  belongs_to :project
  belongs_to :status, class_name: "TaskStatus"

  def start_time
    self.schedule_date.to_datetime
  end

  def end_time
    self.complete_date.to_datetime
  end
end
