# == Schema Information
#
# Table name: task_statuses
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TaskStatus < ApplicationRecord
  validates_presence_of :name
  
  has_many :tasks
end
