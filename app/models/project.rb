# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  description :text
#  client_id   :integer
#  type_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Project < ApplicationRecord
  include HistoricalOwner

  validates :client, :type, presence: true

  belongs_to :client
  belongs_to :type
end
