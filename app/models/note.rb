# == Schema Information
#
# Table name: notes
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  message       :text
#  noteable_id   :integer
#  noteable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Note < ApplicationRecord
  validates :user, presence: true
  validates :noteable, presence: true
  validates_presence_of :message

  belongs_to :user
  belongs_to :noteable, polymorphic: true
end
