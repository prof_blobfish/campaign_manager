require 'active_support/concern'

module HistoricalOwner
  extend ActiveSupport::Concern

  def owner=(new_owner)
    if new_owner.present? && new_owner.instance_of?(User)
      self.historicals.create(user: new_owner)
    end
  end

  def owner
    historicals.last.user if historicals.last.present?
  end

  def owner_id=(new_owner_id)
    new_owner = User.find_by(id: new_owner_id)
    self.owner = new_owner
  end

  def owner_id
    owner.id
  end

  def owner_history
    historicals.order('created_at ASC')
  end

  included do
    has_many :historicals, as: :item
  end
end
