class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.datetime :schedule_date
      t.datetime :complete_date
      t.references :task_status, foreign_key: true
      t.references :client, foreign_key: true
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
