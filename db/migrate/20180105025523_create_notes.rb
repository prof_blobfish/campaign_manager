class CreateNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :notes do |t|
      t.references :user, foreign_key: true
      t.text :message
      t.integer :noteable_id
      t.string :noteable_type

      t.timestamps
    end
  end
end
