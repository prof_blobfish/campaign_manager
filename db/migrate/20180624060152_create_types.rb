class CreateTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :types do |t|
      t.string :name
      t.text :description
      t.integer :type_id
      t.datetime :created_at, null: false

      t.timestamps
    end
  end
end
